#lang racket

(require
 racket/contract
 racket/gui)

(require
 "../src/drawing-classes.rkt"
 "../src/textures.rkt"
 "../src/define-contract-doc.rkt"
 "../src/gui.rkt")


(define atlas3
  (texture-layout->texture-atlas
   '((image "assets/test3.png" width 30 height 10)
     ((height 10
              (("pic1" 10)
               ("pic2" 10)
               ("pic3" 10)))))))

(define ds
  (list
   (new drawable%
        [texture (first (get-field textures atlas3))]
        [x 0.1] [y 0.1] [w 0.1] [h 0.1])
   (new drawable%
        [texture (second (get-field textures atlas3))]
        [x 0.3] [y 0.3] [w 0.2] [h 0.2])
   (new drawable%
        [texture (third (get-field textures atlas3))]
        [x 0.6] [y 0.6] [w 0.3] [h 0.3])))

(define scene-layer
  (new scene-layer% [drawables ds]))

(define frame
  (new frame%
       [label "testing"]
       [min-width 800]
       [min-height 600]
       [x 1500]
       [y 800]))

(define game-canvas
  (new game-canvas%
       [parent frame]
       [scene-layers (list scene-layer)]))

(define (rand wh)
  (/ (random (exact-round (- 100 (* 100 wh)))) 100.0))

(define (frame-thunk)
  (for ([d ds])
    (let ([x (get-field x d)]
          [y (get-field y d)]
          [w (get-field w d)]
          [h (get-field h d)])
      (if (> 0 (+ y h))
          (let ([random-x (rand w)])
            (set-field! x d random-x)
            (set-field! y d (+ 1.0 h)))
          (set-field! y d (- y 0.01))))))

(define (main)
  (send frame show #t)
  (send game-canvas main-loop-start frame-thunk))

(module+ main
  (main))

#lang info

(define name "game123")
(define scribblings '(("scrbl/main.scrbl" ())))
(define collection "game123")
(define deps
  (list
   "base"
   "gui-lib"
   "sgl"
   "pict-lib"
   "scribble-lib"
   ;;"rackunit-lib"
   ))

(define build-deps
  (list
   "scribble-lib"
   "racket-doc"))
